import { generateHwCode } from '../../src/utils';

describe('Utils function tests', () => {
   it('generateHwCode should return HwCode', async () => {
       const actual = generateHwCode();
       expect(actual).toEqual(
           expect.stringMatching(/^#1-([\da-z]+:){2}([\da-z]+-){5}([\da-z]+)$/)
       )
   })
});
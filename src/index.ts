#!/usr/bin/env node


import minimist from 'minimist';
import auth from "./commands/auth";
import test from "./commands/test"

const commandMap: {[key: string]: Function} = {
    auth,
    test
};

(async () => {
    try {
        const argv = minimist(process.argv.slice(2));

        if (commandMap[argv._[0]]) {
            await commandMap[argv._[0]](argv);
            process.exit();
        } else {

        }
    } catch(err) {
        console.error(err);
        process.exit();
    }
})();
import {
    loginRequest,
} from './api/launcher';

import {
    LoginPayload,
    HardwareCodeActivatePayload,
} from './types/api/launcher';

import {set} from "./storage";
import {findOrCreateHwCode} from "./utils";

export async function login(payload: LoginPayload) {
    const response = await loginRequest({
        email: payload.email,
        pass: payload.password,
        hwCode: await findOrCreateHwCode(),
        captcha: null
    });
    console.log("Loggin succeessfull")

    await set('auth.accessToken', response.data.access_token);

    await set('auth.refreshToken', response.data.refresh_token);

    await set('auth.tokenExpiration', Date.now() + (response.data.expires_in * 1000));

    return response;
}
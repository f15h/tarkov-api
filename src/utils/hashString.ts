import { createHash } from 'crypto';


export function hashString(s: string)  {
    return createHash('md5')
        .update(s)
        .digest('hex')
}

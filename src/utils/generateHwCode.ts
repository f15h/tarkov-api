import {
    getRandomHash
} from "./getRandomHash";

export function generateHwCode(): string {
    return `#1-${getRandomHash()}:${getRandomHash()}:${getRandomHash()}-${getRandomHash()}-${getRandomHash()}-${getRandomHash()}-${getRandomHash()}-${getRandomHash(true)}`
}